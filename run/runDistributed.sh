#!/bin/bash

echo Remote player output:
java -jar ping_pong-0.0.1.jar distributedGame_2ndplayer.json & 
# We will simply kill remote player process because it has no stop condition itself. It's like a server
playerPID=$!

# Initiator has stop condition
java -jar ping_pong-0.0.1.jar distributedGame_initiator.json > initiator.out.txt
echo Initiator output:
cat initiator.out.txt


kill -KILL $playerPID
