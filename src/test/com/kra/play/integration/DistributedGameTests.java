package com.kra.play.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.junit.Test;

import com.kra.play.infra.Message;
import com.kra.play.infra.Player;
import com.kra.play.infra.remote.NioMessagingContext;
import com.kra.play.infra.remote.RemotePlayer;
import com.kra.play.integration.tools.TestingMessageRouter;
import com.kra.play.integration.tools.TestingPlayer;
import com.kra.play.pingpong.InitMessage;
import com.kra.play.pingpong.PingPongInitiator;
import com.kra.play.pingpong.PingPongMessage;
import com.kra.play.pingpong.PingPongPlayer;
import com.kra.play.util.ByteSlice;

public class DistributedGameTests {

	private final int testContextPort = 3333;
	private final int remoteContextPort = 4444;
	private NioMessagingContext testingContext;
	private TestingPlayer qa;
	private final short initiatorPlayerId = (short)0;
	private final short player2Id = (short)1;
	private NioMessagingContext remoteContext;

	private void createRemoteContext() throws InterruptedException {
		final Player[] localPlayers = new Player[] { new PingPongPlayer(this.player2Id) };

		final RemotePlayer initiator = new RemotePlayer(this.initiatorPlayerId, new InetSocketAddress("localhost", this.testContextPort));
		final RemotePlayer[] remotePlayers = new RemotePlayer[] { initiator };

		this.remoteContext = new NioMessagingContext(localPlayers, remotePlayers, this.remoteContextPort);
		
		final Thread serverThread = new Thread(() -> {
			try {
				this.remoteContext.startReceiver();
			} catch (final IOException e) {
				e.printStackTrace();
				fail("Unable to start");
			}
		});

		serverThread.start();
		this.remoteContext.waitForStart();
	}
	
	@Test
	public void secondPlayeriRemoteTestCase() throws InterruptedException {
		// Initialize remote
		this.createRemoteContext();

		this.createQAPlayer();
		
		final PingPongInitiator initiator = new PingPongInitiator((short) 0);
	
		final Player[] localPlayers = new Player[] { initiator, this.qa };

		final RemotePlayer player2 = new RemotePlayer(this.player2Id, new InetSocketAddress("localhost", this.remoteContextPort));
		final RemotePlayer[] remotePlayers = new RemotePlayer[] { player2 };

		this.testingContext = new NioMessagingContext(localPlayers, remotePlayers, this.testContextPort);
		this.testingContext.setShutDownListener(cntx -> {
			this.remoteContext.shutDown();
			synchronized (this.qa) {
				this.qa.notifyAll();
			}
		});

		final Thread serverThread = new Thread(() -> {
			try {
				this.testingContext.startReceiver();
			} catch (final IOException e) {
				e.printStackTrace();
				fail("Unable to start");
			}
		});

		serverThread.start();
		this.testingContext.waitForStart();
		
		this.testingContext.getRouters().addFirst(new TestingMessageRouter(this.testingContext, this.qa));

		final int stopCondition = 10;
		final Message initMsg = new InitMessage((short) -1, initiator.getId(), this.player2Id, stopCondition, ByteSlice.fromString("Ping"));
		this.testingContext.send(initMsg);

		// wait for success or fail outcome
		synchronized (this.qa) {
			this.qa.wait();
		}

		assertTrue("A message expected but not received", this.qa.getValidators().isEmpty());
		this.qa.checkAssertion();
	}

	private void createQAPlayer() {

		this.qa = new TestingPlayer((short) 2);
		this.qa.expect((InitMessage msg) -> {
			assertEquals(10, msg.getStopCondition());
			assertEquals(this.initiatorPlayerId, msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping"), msg.getInitTextMessage());
		});
		this.qa.expect((PingPongMessage msg) -> {
			assertEquals(this.player2Id, msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping"), msg.getText());
		});
		this.qa.expect((PingPongMessage msg) -> {
			assertEquals(this.initiatorPlayerId, msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping0"), msg.getText());
		});
		this.qa.expect((PingPongMessage msg) -> {
			assertEquals(this.player2Id, msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping00"), msg.getText());
		});
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455667"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566778"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455667788"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677889"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566778899"), msg.getText()));
		this.qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677889910"), msg.getText()));

		// error outcome
		this.qa.setOnError(err -> {
			synchronized (this.qa) {
				this.qa.notifyAll();
				throw err;
			}
		});
	}

}
