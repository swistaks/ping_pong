package com.kra.play.integration;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

import org.junit.Test;

import com.kra.play.infra.DuplicatingRouter;
import com.kra.play.infra.LocalMessagingContext;
import com.kra.play.infra.LoggingPlayer;
import com.kra.play.infra.Message;
import com.kra.play.integration.tools.TestingMessageRouter;
import com.kra.play.integration.tools.TestingPlayer;
import com.kra.play.pingpong.InitMessage;
import com.kra.play.pingpong.PingPongInitiator;
import com.kra.play.pingpong.PingPongMessage;
import com.kra.play.pingpong.PingPongPlayer;
import com.kra.play.util.ByteSlice;

public class GameTests {

	@Test
	public void localGameTestCase() {
		final PingPongInitiator initiator = new PingPongInitiator((short) 0);
		final PingPongPlayer player2 = new PingPongPlayer((short) 1);

		final TestingPlayer qa = new TestingPlayer((short) 2);
		final int stopCondition = 10;
		qa.expect((InitMessage msg) -> {
			assertEquals(stopCondition, msg.getStopCondition());
			assertEquals(initiator.getId(), msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping"), msg.getInitTextMessage());
		});
		qa.expect((PingPongMessage msg) -> {
			assertEquals(player2.getId(), msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping"), msg.getText());
		});
		qa.expect((PingPongMessage msg) -> {
			assertEquals(initiator.getId(), msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping0"), msg.getText());
		});
		qa.expect((PingPongMessage msg) -> {
			assertEquals(player2.getId(), msg.getDestinationId());
			assertEquals(ByteSlice.fromString("Ping00"), msg.getText());
		});
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455667"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566778"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping001122334455667788"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677889"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping00112233445566778899"), msg.getText()));
		qa.expect((PingPongMessage msg) -> assertEquals(ByteSlice.fromString("Ping0011223344556677889910"), msg.getText()));

		final LocalMessagingContext context = new LocalMessagingContext(initiator, player2, qa);

		context.getRouters().addFirst(new TestingMessageRouter(context, qa));

		final Message initMsg = new InitMessage((short) -1, initiator.getId(), player2.getId(), stopCondition, ByteSlice.fromString("Ping"));
		context.send(initMsg);

		context.waitAllTasksDone();

		assertTrue("A message expected but not received", qa.getValidators().isEmpty());
		qa.checkAssertion();
	}

	@Test
	public void asyncOutputTest() {
		final PingPongInitiator initiator = new PingPongInitiator((short) 0);
		final PingPongPlayer player2 = new PingPongPlayer((short) 1);

		final ByteArrayOutputStream binOutput = new ByteArrayOutputStream();
		final PrintStream ps = new PrintStream(binOutput);

		final LoggingPlayer logger = new LoggingPlayer((short) 2, ps);

		final LocalMessagingContext context = new LocalMessagingContext(initiator, player2, logger);

		context.getRouters().addFirst(new DuplicatingRouter(context.getSender(), logger.getId()));

		final Message initMsg = new InitMessage((short) -1, initiator.getId(), player2.getId(), 10, ByteSlice.fromString("Ping"));
		context.send(initMsg);

		context.waitAllTasksDone();

		final String content = new String(binOutput.toByteArray(), StandardCharsets.UTF_8);

		// Check some output parts
		assertTrue(content.contains("Ping\n"));
		assertTrue(content.contains("Ping0\n"));
		assertTrue(content.contains("Ping001122334\n"));
		assertTrue(content.contains("Ping0011223344556677889910\n"));
	}

}
