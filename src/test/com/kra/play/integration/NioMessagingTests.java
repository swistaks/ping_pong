package com.kra.play.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.junit.Test;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessageSender;
import com.kra.play.infra.Player;
import com.kra.play.infra.SimplePlayerResolver;
import com.kra.play.infra.remote.NioMessageReceiver;
import com.kra.play.infra.remote.NioMessageSender;
import com.kra.play.infra.remote.RemotePlayer;
import com.kra.play.pingpong.InitMessage;
import com.kra.play.pingpong.PingPongMessageFactory;
import com.kra.play.pingpong.PingPongPlayer;
import com.kra.play.util.ByteSlice;

public class NioMessagingTests {

	private static final int port = 1111;
	private NioMessageReceiver nioReceiver;
	private AssertionError err;

	@Test
	public void sendReceiveTest() throws InterruptedException {

		// ----- Remote part
		final MessageSender testingSender = new MessageSender() {

			@Override
			public boolean send(Message message) {
				try {
					assertTrue(message instanceof InitMessage);
					message.getBuffer().flip();
					final InitMessage initM = (InitMessage) message;
					assertEquals(ByteSlice.fromString("Ping"), initM.getInitTextMessage());
				} catch (final AssertionError e) {
					NioMessagingTests.this.err = e;
				}

				try {
					NioMessagingTests.this.nioReceiver.shutDown();
				} catch (final IOException e) {
					e.printStackTrace();
					fail("Unable to shut down");
				}
				return false;
			}

		};

		this.nioReceiver = new NioMessageReceiver(testingSender, port, new PingPongMessageFactory());
		final Thread serverThread = new Thread(() -> {
			try {
				this.nioReceiver.runServer();
			} catch (final IOException e) {
				e.printStackTrace();
				fail("Unable to start");
			}
		});

		serverThread.start();
		this.nioReceiver.waitForStart();

		// ----- Client part

		final RemotePlayer remoteInitiator = new RemotePlayer((short) 0, new InetSocketAddress("localhost", 1111));
		final PingPongPlayer player2 = new PingPongPlayer((short) 1);

		final Message initMsg = new InitMessage((short) -1, remoteInitiator.getId(), player2.getId(), 10, ByteSlice.fromString("Ping"));

		final NioMessageSender nioSender = new NioMessageSender(new SimplePlayerResolver(new Player[] { remoteInitiator }));
		nioSender.send(initMsg);

		// -- Join and finish 
		
		serverThread.join();
		if (this.err != null) {
			throw this.err;
		}
	}

}
