package com.kra.play.integration.tools;

import static org.junit.Assert.assertFalse;

import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Consumer;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessagingContext;
import com.kra.play.infra.Player;

/**
 * Checks incoming messages using a list of predefined validators.
 *
 */
public class TestingPlayer extends Player {

	private final Deque<MessageValidator<? extends Message>> validators = new LinkedList<>();
	private AssertionError error;
	private Consumer<AssertionError> onError;

	public TestingPlayer(short id) {
		super(id);
	}

	public <T extends Message> void expect(Consumer<T> assertion) {
		this.getValidators().addLast(new MessageValidator<>(assertion));
	}

	public void receive(Message message, MessagingContext context) {
		if (this.getValidators().isEmpty()) {
			assertFalse("Unexpected message", this.getValidators().isEmpty());
		}
		try {
			this.getValidators().pollFirst().validate(message);
		} catch (final AssertionError err) {
			this.error = err;
			if (this.getOnError() != null) {
				this.getOnError().accept(err);
			}
		}
	}

	public Deque<MessageValidator<? extends Message>> getValidators() {
		return this.validators;
	}

	public void checkAssertion() {
		if (this.error != null) {
			throw this.error;
		}
	}

	public Consumer<AssertionError> getOnError() {
		return this.onError;
	}

	public void setOnError(Consumer<AssertionError> onError) {
		this.onError = onError;
	}
}
