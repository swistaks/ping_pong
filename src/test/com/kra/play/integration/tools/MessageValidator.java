package com.kra.play.integration.tools;

import java.util.function.Consumer;

import com.kra.play.infra.Message;

public class MessageValidator<T extends Message> {

	private final Consumer<T> assertion;

	public MessageValidator(Consumer<T> assertion) {
		this.assertion = assertion;
	}

	@SuppressWarnings("unchecked")
	void validate(Message message) {
		this.assertion.accept((T) message);
	}
}
