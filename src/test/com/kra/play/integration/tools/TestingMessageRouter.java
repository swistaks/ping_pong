package com.kra.play.integration.tools;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessageRouter;
import com.kra.play.infra.MessagingContext;

public class TestingMessageRouter implements MessageRouter {

	private final MessagingContext context;
	private final TestingPlayer qa;

	public TestingMessageRouter(MessagingContext context, TestingPlayer qa) {
		super();
		this.context = context;
		this.qa = qa;
	}

	@Override
	public boolean route(Message message) {
		this.qa.receive(message, this.context);
		return true;
	}
}
