package com.kra.play.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class ByteSliceTest {

	@Test
	public void equalsTest() {
		final byte [] sample = "ABCDE".getBytes();
		assertTrue(ByteSlice.fromString("ABC").equals(new ByteSlice().wrap(sample, 0, 3)));
		assertTrue(ByteSlice.fromString("BC").equals(new ByteSlice().wrap(sample, 1, 2)));
		assertTrue(ByteSlice.fromString("CDE").equals(new ByteSlice().wrap(sample, 2, 3)));
		assertTrue(ByteSlice.fromString("").equals(new ByteSlice().wrap(sample, 2, 0)));

		assertFalse(ByteSlice.fromString("").equals(null));
		assertFalse(ByteSlice.fromString("ABC").equals(new ByteSlice().wrap(sample, 0, 4)));
		assertFalse(ByteSlice.fromString("ABC").equals(new ByteSlice().wrap(sample, 0, 2)));
	}

	@Test
	public void hashcodeTest() {
		final byte [] sample = "ABCDE".getBytes();
		assertEquals(ByteSlice.fromString("ABC").hashCode(), (new ByteSlice().wrap(sample, 0, 3)).hashCode());
		assertEquals(ByteSlice.fromString("BC").hashCode(), (new ByteSlice().wrap(sample, 1, 2)).hashCode());
		assertEquals(ByteSlice.fromString("CDE").hashCode(), (new ByteSlice().wrap(sample, 2, 3)).hashCode());
		assertEquals(ByteSlice.fromString("").hashCode(), (new ByteSlice().wrap(sample, 2, 0)).hashCode());

		// This must not be always true according to hashcode() contract, but it is for current implementation 
		assertNotEquals(ByteSlice.fromString("").hashCode(), 0);
		assertNotEquals(ByteSlice.fromString("ABC").hashCode(), (new ByteSlice().wrap(sample, 0, 4)).hashCode());
		assertNotEquals(ByteSlice.fromString("ABC").hashCode(), (new ByteSlice().wrap(sample, 0, 2)).hashCode());
	}
	
	@Test
	public void putUnsignedAsCharsTest() {
		final ByteSlice slice = ByteSlice.allocate(20);
		assertEquals(slice.putUnsignedAsChars(0), ByteSlice.fromString("0"));
		assertEquals(slice.putUnsignedAsChars(9), ByteSlice.fromString("9"));
		assertEquals(slice.putUnsignedAsChars(11), ByteSlice.fromString("11"));
		assertEquals(slice.putUnsignedAsChars(1234567890), ByteSlice.fromString("1234567890"));
		assertEquals(slice.putUnsignedAsChars(Integer.MAX_VALUE), ByteSlice.fromString(Integer.toString(Integer.MAX_VALUE)));
		assertEquals(slice.putUnsignedAsChars(-1), ByteSlice.fromString("1"));
	}

}
