package com.kra.play;

import java.util.List;

import com.kra.play.infra.LocalMessagingContext;
import com.kra.play.infra.Message;

/**
 * This class starts messaging context with local players only. The
 * startContext method blocks current thread till one of the players call
 * shutDown() on context (stop condition occurs).
 *
 */
public class LocalContextManager implements MessagingContextManager {

	private final LocalMessagingContext context;
	private final List<Message> initMessages;

	public LocalContextManager(LocalMessagingContext context, List<Message> initMessages) {
		this.context = context;
		this.initMessages = initMessages;
	}

	@Override
	public void startContext() throws InterruptedException {
		final Object syncRoot = new Object();

		this.context.setShutDownListener(c -> {
			synchronized (syncRoot) {
				syncRoot.notifyAll();
			}
		});
		
		for (final Message msg : this.initMessages) {
			this.context.send(msg);
		}

		synchronized (syncRoot) {
			syncRoot.wait();
		}

		this.context.waitAllTasksDone();
	}

}
