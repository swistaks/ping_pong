package com.kra.play;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.kra.play.config.ContextConfig;

/**
 * Console application launcher
 *
 */
public class Launchpad {

	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException {

		if (args.length == 0) {
			System.out.println("missing argument: path to configuration file (json)");
			System.exit(1);
		}

		try (FileReader confReader = new FileReader(args[0])) {
			final ContextConfig config = new Gson().fromJson(confReader, ContextConfig.class);
			final ConfigurableContextBuilder builder = new ConfigurableContextBuilder(config);
			final MessagingContextManager contextManager = builder.buildContextManager();
			contextManager.startContext();
		}
	}

}
