package com.kra.play;

/**
 * This class is responsible for context starting 
 *
 */
public interface MessagingContextManager {

	void startContext() throws InterruptedException;
	
}
