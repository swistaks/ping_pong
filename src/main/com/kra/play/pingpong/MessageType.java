package com.kra.play.pingpong;

/**
 * Dictionary of message codes.
 * 
 * A candidate for code generation.
 *
 */
public enum MessageType {
	//TODO: enum is not a good solution because cannot be decomposed by domains
	Unknown((short) -1), PingPongMessage((short) 0), InitMessage((short) 1);

	private final short messageTypeCode;

	MessageType(short messageTypeCode) {
		this.messageTypeCode = messageTypeCode;
	}

	public short getMessageTypeCode() {
		return this.messageTypeCode;
	}

	public static MessageType getByCode(short messageTypeCode) {
		for (final MessageType type : MessageType.values()) {
			if (type.getMessageTypeCode() == messageTypeCode) {
				return type;
			}
		}
		return MessageType.Unknown;
	}
}
