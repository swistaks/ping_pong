package com.kra.play.pingpong;

import java.nio.ByteBuffer;

import com.kra.play.infra.AbstractBinaryMessageFactory;
import com.kra.play.infra.Message;

/**
 * Creates Message by its binary representation (for Ping Pong game domain).
 * 
 * A candidate for code generation.
 *
 */
public final class PingPongMessageFactory extends AbstractBinaryMessageFactory {

	@Override
	public Message createMessage(ByteBuffer binaryMessage) {
		final MessageType messageType = Message.getMessageType(binaryMessage);
		switch (messageType) {
			case InitMessage:
				return new InitMessage().wrap(binaryMessage);
			case PingPongMessage:
				return new PingPongMessage().wrap(binaryMessage);
			case Unknown:
			default:
				return null;
		}
	}

}
