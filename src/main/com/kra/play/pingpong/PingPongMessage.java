package com.kra.play.pingpong;

import java.io.PrintStream;

import com.kra.play.infra.Message;
import com.kra.play.util.ByteSlice;

/**
 * Main message in the Ping Pong game.
 * Contains message text and methods for adding count to it. 
 *
 */
public class PingPongMessage extends Message {

	private interface Offsets {
		byte Text = 0;
	}
	
	private final ByteSlice textSlice = new ByteSlice();
	private final ByteSlice digitsSlice = ByteSlice.allocate(10);
	
	public PingPongMessage() {
		super();
	}
	
	public PingPongMessage(short senderId, short destinationId, ByteSlice textMessage, int estimatedSize) {
		super(MessageType.PingPongMessage, getSize(textMessage, estimatedSize));
		this.getBuffer().position(this.getPayloadOffset() + Offsets.Text);
		this.setSenderId(senderId);
		this.setDestinationId(destinationId);
		this.appendText(textMessage);
	}

	private static int getSize(ByteSlice textMessage, int estimatedSize) {
		return Math.max(Offsets.Text + textMessage.getLength(), estimatedSize);
	}
	
	public ByteSlice getText() {
		final int textOffset = this.getPayloadOffset() + Offsets.Text;
		return this.textSlice.wrap(this.getBuffer(), textOffset, this.getBuffer().position() - textOffset);
	}

	public void appendText(ByteSlice textMessage) {
		//TODO: here we need to reallocate buffer if there is no space remaining
		textMessage.writeTo(this.getBuffer());
	}

	public void appendIntAsChars(int value) {
		this.digitsSlice.putUnsignedAsChars(value);
		this.appendText(this.digitsSlice);
	}

	@Override
	public void toStream(PrintStream out) {
		super.toStream(out);
		out.print("\tText: ");
		this.getText().toStream(out);
	}

}
