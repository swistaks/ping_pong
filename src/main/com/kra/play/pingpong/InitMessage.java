package com.kra.play.pingpong;

import java.io.PrintStream;

import com.kra.play.infra.Message;
import com.kra.play.util.ByteSlice;

/**
 * This message initializes Ping Pong game
 *
 */
public class InitMessage extends Message {

	private interface Offsets {
		byte TargetPlayerId = 0;
		byte StopCondition = TargetPlayerId + Short.BYTES;
		byte InitTextMessage = StopCondition + Integer.BYTES; 
	}

	private final ByteSlice initTextMessage = new ByteSlice(); 
	
	public InitMessage() {
		super();
	}
	
	public InitMessage(short senderId, short destinationId, short targetPlayerId, int stopCondition, ByteSlice initTextMessage) {
		super(MessageType.InitMessage, Offsets.InitTextMessage + initTextMessage.getLength());
		this.setSenderId(senderId);
		this.setDestinationId(destinationId);
		this.setTergetPlayerId(targetPlayerId);
		this.setStopCondition(stopCondition);
		this.setInitTextMessage(initTextMessage);
	}

	public short getTargetPlayerId() {
		return this.getBuffer().getShort(this.getPayloadOffset() + Offsets.TargetPlayerId);
	}

	private void setTergetPlayerId(short targetPlayerId) {
		this.getBuffer().putShort(this.getPayloadOffset() + Offsets.TargetPlayerId, targetPlayerId);
	}

	public int getStopCondition() {
		return this.getBuffer().getInt(this.getPayloadOffset() + Offsets.StopCondition);
	}

	private void setStopCondition(int stopCondition) {
		this.getBuffer().putInt(this.getPayloadOffset() + Offsets.StopCondition, stopCondition);
	}

	public ByteSlice getInitTextMessage() {
		final int textOffset = this.getPayloadOffset() + Offsets.InitTextMessage;
		return this.initTextMessage.wrap(this.getBuffer(), textOffset, this.getBuffer().limit() - textOffset);
	}

	private void setInitTextMessage(ByteSlice initTextMessage) {
		final int textOffset = this.getPayloadOffset() + Offsets.InitTextMessage;
		initTextMessage.writeTo(this.getBuffer(), textOffset);
	}

	@Override
	public void toStream(PrintStream out) {
		super.toStream(out);
		out.print("\tText: ");
		this.getInitTextMessage().toStream(out);
	}
	
}
