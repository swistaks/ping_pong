package com.kra.play.pingpong;

import java.util.concurrent.atomic.AtomicInteger;

import com.kra.play.infra.MessagingContext;

/**
 * A player which initiates Ping Pong game.
 * Can receive InitMessage and start the game.
 * Also as a simple player it receives and sends PingPongMesssage.
 * Able to stop the game on stop condition. 
 *
 */
public class PingPongInitiator extends PingPongPlayer {

	private final AtomicInteger messagesReceived = new AtomicInteger();

	// We do not expect concurrent init messages 
	private int stopCondition;

	public PingPongInitiator(short id) {
		super(id);
	}

	public void receive(InitMessage initMessage, MessagingContext context) {
		this.stopCondition = initMessage.getStopCondition();

		final int avgDigitsCount = 5;
		final int totalDigitsAppended = initMessage.getStopCondition() * avgDigitsCount;
		final int estimatedPingSize = initMessage.getInitTextMessage().getLength() + totalDigitsAppended;
		final PingPongMessage firstPing = new PingPongMessage(
				this.getId(), 
				initMessage.getTargetPlayerId(), 
				initMessage.getInitTextMessage(), 
				estimatedPingSize);

		context.send(firstPing);
	}

	@Override
	public void receive(PingPongMessage inMessage, MessagingContext context) {
		final int msgReceived = this.messagesReceived.getAndIncrement();
		if (this.getMessagesSent() >= this.stopCondition && msgReceived >= this.stopCondition) {
			context.shutDown();
		} else {
			super.receive(inMessage, context);
		}
	}

}
