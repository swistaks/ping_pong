package com.kra.play.pingpong;

import com.kra.play.infra.AbstractMessagingContext;
import com.kra.play.infra.InfraMessageDispatcher;
import com.kra.play.infra.Message;
import com.kra.play.infra.Player;

/**
 *
 * Message dispatcher for Ping Pong game domain
 * 
 * The class is a good candidate for code generation.
 *
 */
public class PingPongMessageDispatcher extends InfraMessageDispatcher {

	public PingPongMessageDispatcher(AbstractMessagingContext context) {
		super(context);
	}

	@Override
	public boolean dispatch(Player player, Message message) {
		if (player instanceof PingPongInitiator) {
			return this.dispatchToPingPongInitiator((PingPongInitiator) player, message);
		} else if (player instanceof PingPongPlayer) {
			return this.dispatchToPingPongPlayer((PingPongPlayer) player, message);
		} else {
			return super.dispatch(player, message);
		}
	}

	private boolean dispatchToPingPongInitiator(PingPongInitiator player, Message message) {
		if (message instanceof InitMessage) {
			player.receive((InitMessage) message, this.context);
			return true;
		}
		return this.dispatchToPingPongPlayer(player, message);
	}

	private boolean dispatchToPingPongPlayer(PingPongPlayer player, Message message) {
		if (message instanceof PingPongMessage) {
			player.receive((PingPongMessage) message, this.context);
			return true;
		}
		return false;
	}
}
