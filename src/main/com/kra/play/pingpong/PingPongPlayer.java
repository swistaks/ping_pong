package com.kra.play.pingpong;

import java.util.concurrent.atomic.AtomicInteger;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessagingContext;
import com.kra.play.infra.Player;

/**
 * Receives PingPongMessage and sends beck response concatenated with current messages sent count
 *
 */
public class PingPongPlayer extends Player {
	
	private final AtomicInteger messagesSent = new AtomicInteger();
	
	public PingPongPlayer(short id) {
		super(id);
	}

	public void receive(PingPongMessage inMessage, MessagingContext context) {
		
		final PingPongMessage response = Message.makeResponse(inMessage);
		
		final int msgSent = this.messagesSent.getAndIncrement();
		response.appendIntAsChars(msgSent);
		
		context.send(response);
	}
	
	public int getMessagesSent() {
		return this.messagesSent.get();
	}
}
