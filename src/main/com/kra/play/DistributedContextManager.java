package com.kra.play;

import java.io.IOException;
import java.util.List;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessagingException;
import com.kra.play.infra.remote.NioMessagingContext;

/**
 * This class runs context with remote messages receiver. The startContext()
 * method blocks the thread till shutDown() call
 *
 */
public class DistributedContextManager implements MessagingContextManager {

	private final NioMessagingContext context;
	private final List<Message> initMessages;

	public DistributedContextManager(NioMessagingContext context, List<Message> initMessages) {
		this.context = context;
		this.initMessages = initMessages;
	}

	@Override
	public void startContext() throws InterruptedException {
		final Object syncRoot = new Object();

		this.context.setShutDownListener(c -> {
			synchronized (syncRoot) {
				syncRoot.notifyAll();
			}
		});

		final Thread receiverThread = new Thread(() -> {
			try {
				this.context.startReceiver();
			} catch (final IOException e) {
				throw new MessagingException("Unable to start receiver", e);
			}
		});

		receiverThread.start();
		this.context.waitForStart();

		for (final Message msg : this.initMessages) {
			this.context.send(msg);
		}

		synchronized (syncRoot) {
			syncRoot.wait();
		}

		this.context.waitAllTasksDone();
		receiverThread.join();
	}

}
