package com.kra.play.infra;

/**
 * High-level interface to messaging system
 *
 */
public interface MessagingContext {

	boolean send(Message message);

	void shutDown();

}