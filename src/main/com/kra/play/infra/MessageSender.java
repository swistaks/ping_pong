package com.kra.play.infra;

/**
 * Responsible for message delivery 
 *
 */
public interface MessageSender {

	public abstract boolean send(Message message);

}
