package com.kra.play.infra;

/**
 * Puts incoming message into a local execution queue. 
 *
 */
public class LocalMessageSender implements MessageSender {

	private final PlayerResolver playerResolver;
	private final ActiveMessageQueue queue;

	public LocalMessageSender(PlayerResolver playerResolver, MessageDispatcher dispatcher) {
		this.playerResolver = playerResolver;
		final MessageHandler handler = new MessageHandler(playerResolver, dispatcher);
		this.queue = new ActiveMessageQueue(handler);
	}

	@Override
	public boolean send(Message message) {
		if (this.playerResolver.resolveDestination(message) != null) {
			this.queue.push(message);
			return true;
		} else {
			return false;
		}
	}

	public void shutDown() {
		this.queue.shutDown();
	}

	public void waitAllTasksDone() {
		this.queue.waitAllTasksDone();
	}
}
