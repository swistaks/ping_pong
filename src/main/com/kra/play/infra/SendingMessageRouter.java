package com.kra.play.infra;

/**
 * This router just sends the message with the sender provided 
 *
 */
public class SendingMessageRouter implements MessageRouter {

	private final MessageSender sender;

	public SendingMessageRouter(MessageSender sender) {
		this.sender = sender;
	}

	@Override
	public boolean route(Message message) {
		this.sender.send(message);
		return true;
	}

}
