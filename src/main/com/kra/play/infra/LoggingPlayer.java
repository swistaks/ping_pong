package com.kra.play.infra;

import java.io.PrintStream;

/**
 * Infrastructure "player". Used to log messages.
 *
 */
public class LoggingPlayer extends Player {

	private final PrintStream ps;

	public LoggingPlayer(short id, PrintStream ps) {
		super(id);
		this.ps = ps;
	}

	public synchronized void receive(Message message, MessagingContext context) {
		message.toStream(this.ps);
		this.ps.println();
	}
}
