package com.kra.play.infra;

/**
 * This class implements message handling logic: It resolves target player
 * instance and delivers a message using dispatcher provided.
 * 
 * This code must be thread safe.
 */
public class MessageHandler {

	private final MessageDispatcher dispatcher;
	private final PlayerResolver playerResolver;

	public MessageHandler(PlayerResolver playerResolver, MessageDispatcher dispatcher) {
		this.playerResolver = playerResolver;
		this.dispatcher = dispatcher;
	}

	public void handleMessage(Message message) {
		final Player player = this.playerResolver.resolveDestination(message);
		if (player == null) {
			throw new MessagingException("Unable to resolve player: " + message.getDestinationId());
		}
		try {
			this.dispatcher.dispatch(player, message);
		} catch (final Exception ex) {
			// TODO: handle exception properly
			ex.printStackTrace();
		}
	}

}
