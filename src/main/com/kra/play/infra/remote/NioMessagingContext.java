package com.kra.play.infra.remote;

import java.io.IOException;

import com.kra.play.infra.CompositeMessageSender;
import com.kra.play.infra.LocalMessageSender;
import com.kra.play.infra.MessageSender;
import com.kra.play.infra.AbstractMessagingContext;
import com.kra.play.infra.Player;
import com.kra.play.infra.SendingMessageRouter;
import com.kra.play.infra.SimplePlayerResolver;
import com.kra.play.pingpong.PingPongMessageDispatcher;
import com.kra.play.pingpong.PingPongMessageFactory;

/**
 * Implements distributed messaging system
 *
 */
public class NioMessagingContext extends AbstractMessagingContext {

	private final MessageSender sender;
	private final LocalMessageSender localMessageSender;
	private final NioMessageSender nioMessageSender;
	private final NioMessageReceiver nioReceiver;

	public NioMessagingContext(Player[] localPlayers, RemotePlayer[] remotePlayers, int serverPort) {
		this.localMessageSender = new LocalMessageSender(new SimplePlayerResolver(localPlayers), 
				new PingPongMessageDispatcher(this));
		this.nioMessageSender = new NioMessageSender(new SimplePlayerResolver(remotePlayers));
		this.sender = new CompositeMessageSender(this.localMessageSender, this.nioMessageSender);
		this.getRouters().add(new SendingMessageRouter(this.sender));

		this.nioReceiver = new NioMessageReceiver(this, serverPort, new PingPongMessageFactory());
	}

	public void startReceiver() throws IOException {
		this.nioReceiver.runServer();
	}

	public void waitForStart() throws InterruptedException {
		this.nioReceiver.waitForStart();
	}
	
	public void waitAllTasksDone() {
		this.localMessageSender.waitAllTasksDone();
	}
	
	@Override
	public void shutDown() {
		this.localMessageSender.shutDown();
		this.nioMessageSender.shutDown();
		try {
			this.nioReceiver.shutDown();
		} catch (final IOException e) {
			// TODO add proper logging
			e.printStackTrace();
		}
		
		super.shutDown();
	}

}
