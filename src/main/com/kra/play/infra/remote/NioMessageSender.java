package com.kra.play.infra.remote;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.kra.play.infra.Message;
import com.kra.play.infra.MessageSender;
import com.kra.play.infra.PlayerResolver;

/**
 * Sends message to remote player via network socket.
 * This part of the solution is quite raw
 *
 */
public class NioMessageSender implements MessageSender {

	private static final int ReconnectTimeout = 1000;

	// TODO We need somekind of heartbeat/keep alive functionality (!)
	// TODO Also every message delivery should be acknowledged (!)
	private static class SocketChannelWrapper {
		SocketChannel channel;
		long lastWriteTime;

		public SocketChannelWrapper(SocketChannel channel, long lastWriteTime) {
			this.channel = channel;
			this.lastWriteTime = lastWriteTime;
		}
	}

	private final PlayerResolver playerResolver;
	private final Map<InetSocketAddress, SocketChannelWrapper> habitatChannels = new ConcurrentHashMap<>();

	public NioMessageSender(PlayerResolver playerResolver) {
		this.playerResolver = playerResolver;
	}

	@Override
	public boolean send(Message message) {
		final RemotePlayer player = (RemotePlayer) this.playerResolver.resolveDestination(message);
		if (player != null) {
			this.send(message, player);
			return true;
		} else {
			return false;
		}
	}

	private void send(Message message, RemotePlayer player) {
		final InetSocketAddress habitat = player.getHabitat();
		final SocketChannelWrapper channelWrapper = this.habitatChannels.computeIfAbsent(habitat, NioMessageSender::openSocket);

		if (channelWrapper != null) {
			final ByteBuffer buffer = message.getBuffer();
			try {
				final SocketChannel channel = this.getChannel(habitat, channelWrapper);
				buffer.flip();
				channel.write(buffer);
			} catch (final IOException e) {
				// TODO this needs to be properly handled
				// implement retry, buffering or returning to initial
				// (quarantine?) queue
				e.printStackTrace();
			}
		}
	}

	private SocketChannel getChannel(InetSocketAddress habitat, SocketChannelWrapper channelWrapper) throws IOException {
		if (System.currentTimeMillis() - channelWrapper.lastWriteTime > ReconnectTimeout) {
			synchronized (channelWrapper) {
				if (System.currentTimeMillis() - channelWrapper.lastWriteTime > ReconnectTimeout) {
					channelWrapper.channel.close();
					channelWrapper.channel = SocketChannel.open(habitat);
					channelWrapper.lastWriteTime = System.currentTimeMillis();
				}
			}
		}
		return channelWrapper.channel;
	}

	private static SocketChannelWrapper openSocket(InetSocketAddress habitat) {
		try {
			return new SocketChannelWrapper(SocketChannel.open(habitat), System.currentTimeMillis());
		} catch (final IOException e) {
			// TODO: log properly
			e.printStackTrace();
			return null;
		}
	}

	public void shutDown() {
		this.habitatChannels.values().forEach(t -> {
			try {
				t.channel.close();
			} catch (final IOException e) {
				// TODO: log properly
				e.printStackTrace();
			}
		});
	}

}
