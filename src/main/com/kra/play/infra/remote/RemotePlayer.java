package com.kra.play.infra.remote;

import java.net.InetSocketAddress;

import com.kra.play.infra.Player;

/**
 * Represents a player on remote host / another process 
 */
public class RemotePlayer extends Player {

	private final InetSocketAddress habitat;

	public RemotePlayer(short id, InetSocketAddress habitat) {
		super(id);
		this.habitat = habitat;
	}

	public InetSocketAddress getHabitat() {
		return this.habitat;
	}

}
