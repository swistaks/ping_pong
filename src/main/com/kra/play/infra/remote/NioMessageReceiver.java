package com.kra.play.infra.remote;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import com.kra.play.infra.AbstractBinaryMessageFactory;
import com.kra.play.infra.Message;
import com.kra.play.infra.MessageReceiver;
import com.kra.play.infra.MessageSender;

/**
 * This class listens for incoming connections and receives messages from remote
 * clients.
 */
public class NioMessageReceiver extends MessageReceiver {

	private static final int MaxMessageSize = 65535;
	private final int receiverPort;
	private final AbstractBinaryMessageFactory messageFactory;
	private final AtomicBoolean isStarted = new AtomicBoolean(false);
	private Selector selector;

	public NioMessageReceiver(MessageSender context, int serverPort, AbstractBinaryMessageFactory messageFactory) {
		super(context);
		this.receiverPort = serverPort;
		this.messageFactory = messageFactory;
	}

	public void shutDown() throws IOException {
		for (final SelectionKey key : this.selector.keys()) {
			key.channel().close();
		}
		this.selector.close();
	}

	public void waitForStart() throws InterruptedException {
		if (!this.isStarted.get()) {
			synchronized (this.isStarted) {
				this.isStarted.wait();
			}
		}
	}

	public void runServer() throws IOException {
		this.selector = Selector.open();

		final ServerSocketChannel socket = ServerSocketChannel.open();

		final InetSocketAddress serverAddress = new InetSocketAddress("localhost", this.receiverPort);
		socket.bind(serverAddress);

		socket.configureBlocking(false);
		socket.register(this.selector, socket.validOps(), null);

		this.notifyStarted();
		System.out.println("Listening on port: " + this.receiverPort);

		while (this.selector.isOpen()) {
			this.selector.select();

			if (this.selector.isOpen()) {
				final Set<SelectionKey> keys = this.selector.selectedKeys();
				final Iterator<SelectionKey> keysIterator = keys.iterator();

				while (keysIterator.hasNext()) {
					final SelectionKey key = keysIterator.next();

					if (!key.isValid()) {
						continue;
					}

					if (key.isAcceptable()) {
						this.acceptClient(this.selector, socket);
					} else if (key.isReadable()) {
						this.readFromClient(key);
					}
					keysIterator.remove();
				}
			}
		}
	}

	private void notifyStarted() {
		this.isStarted.set(true);
		synchronized (this.isStarted) {
			this.isStarted.notifyAll();
		}
	}

	private void readFromClient(final SelectionKey key) throws IOException {
		final ByteBuffer outBuffer = this.getOutBuffer();
		final SocketChannel client = (SocketChannel) key.channel();
		outBuffer.clear();
		final int bytesRead = client.read(outBuffer);
		if (bytesRead < 0) {
			key.channel().close();
			return;
		}

		this.sendMessage(outBuffer);
	}

	private void sendMessage(ByteBuffer binaryMessage) {
		final Message message = this.messageFactory.createMessage(binaryMessage);
		if (message != null) {
			this.getSender().send(message);
		} else {
			// TODO handle properly
			System.err.println("Unknown message: " + binaryMessage);
		}
	}

	private void acceptClient(final Selector selector, final ServerSocketChannel socket) throws IOException, ClosedChannelException {
		final SocketChannel client = socket.accept();
		client.configureBlocking(false);
		client.register(selector, SelectionKey.OP_READ);
	}

	private ByteBuffer getOutBuffer() {
		return ByteBuffer.allocate(MaxMessageSize);
	}

}
