package com.kra.play.infra;

import java.nio.ByteBuffer;

import com.kra.play.util.ByteTools;

/**
 * Creates concrete message instance basing on its binary representation 
 *
 */
public abstract class AbstractBinaryMessageFactory {

	public abstract Message createMessage(ByteBuffer binaryMessage);

	public final Message createCopy(Message message) {
		return this.createMessage(ByteTools.clone(message.getBuffer()));
	}

}