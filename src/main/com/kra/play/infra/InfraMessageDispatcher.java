package com.kra.play.infra;

/**
 * Implements dispatching of messages to infrastructure players, such as logger. 
 *
 */
public class InfraMessageDispatcher extends MessageDispatcher {

	protected final AbstractMessagingContext context;

	public InfraMessageDispatcher(AbstractMessagingContext context) {
		this.context = context;
	}

	@Override
	public boolean dispatch(Player player, Message message) {
		if (player instanceof LoggingPlayer) {
			((LoggingPlayer) player).receive(message, this.context);
			return true;
		}
		return false;
	}

}
