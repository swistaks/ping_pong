package com.kra.play.infra;

/**
 * Performs some actions on incoming message. It can just send the message using
 * a sender, enrich message, filter-out, or create a duplicate message and sent
 * it to different player.
 *
 */
public interface MessageRouter {

	/**
	 * @return false if routing should be stopped
	 */
	boolean route(Message message);

}
