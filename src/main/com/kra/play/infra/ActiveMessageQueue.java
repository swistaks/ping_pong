package com.kra.play.infra;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Represents message handling queue. Encapsulates thread pool which executes
 * specified handler for each message in the pool. Usually handler
 *
 */
public class ActiveMessageQueue {
	private final ExecutorService executor;
	private final MessageHandler handler;

	public ActiveMessageQueue(MessageHandler handler) {
		this.executor = Executors.newWorkStealingPool();
		this.handler = handler;
	}

	public void push(Message message) {
		message.bound(this.handler);
		this.executor.execute(message.getRunnableHandle());
	}

	public void shutDown() {
		this.executor.shutdown();
	}

	public void waitAllTasksDone() {
		try {
			this.executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (final InterruptedException e) {
			System.err.println("Message processing was interrupted.");
		}
	}
}
