package com.kra.play.infra;

/**
 * Resolves player by id 
 *
 */

public abstract class PlayerResolver {

	public abstract Player getById(short playerId);

	public Player resolveDestination(Message message) {
		return this.getById(message.getDestinationId());
	}

}
