package com.kra.play.infra;

import com.kra.play.pingpong.PingPongMessageFactory;

/**
 * Creates copy of the message and sends it to specified player. 
 *
 */
public class DuplicatingRouter implements MessageRouter {

	private final MessageSender sender;
	private final PingPongMessageFactory factory  = new PingPongMessageFactory();
	private final short targetPlayerId;

	public DuplicatingRouter(MessageSender sender, short targetPlayerId) {
		this.sender = sender;
		this.targetPlayerId = targetPlayerId;
	}

	@Override
	public boolean route(Message message) {
		final Message copy = this.factory.createCopy(message);
		copy.setDestinationId(this.targetPlayerId);
		this.sender.send(copy);
		return true;
	}

}
