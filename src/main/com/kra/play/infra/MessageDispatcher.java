package com.kra.play.infra;

/**
 * This class knows which method of the Player can handle this Message.
 *
 * Motivation: decouple message and player since potentially player can handle
 * multiple message types and message can be handled by multiple players.
 */
public abstract class MessageDispatcher {

	public abstract boolean dispatch(Player player, Message message);

}
