package com.kra.play.infra;

/**
 * Throws by messaging system
 */
public class MessagingException extends RuntimeException {

	private static final long serialVersionUID = 2970876510502177769L;

	public MessagingException() {
		super();
	}

	public MessagingException(String message) {
		super(message);
	}

	public MessagingException(String message, Exception e) {
		super(message, e);
	}

}
