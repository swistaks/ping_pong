package com.kra.play.infra;

import java.io.PrintStream;
import java.nio.ByteBuffer;

import com.kra.play.pingpong.MessageType;

/**
 *  Base class for all messages.
 *  Wraps binary buffer with message representation and allows getting and setting message header data.
 *	A Message can be bound to a handler and put into message queue (using runnableHandle). 
 *
 */
public abstract class Message {

	private interface HeaderOffsets {
		byte SenderId = 0;
		byte DestinationId = SenderId + Short.BYTES;
		byte MessageType = DestinationId + Short.BYTES;
		byte PayloadOffset = MessageType + Short.BYTES;
	}

	private ByteBuffer buffer;
	private MessageHandler handler;

	// Since message is not quite runnable it needs a wrapper to be handled by executor service.
	// This is a way to avoid wrapper creation. So the object can be reused for response.
	// TODO probably make Message Runnable?
	private final Runnable runnableHandle = () -> this.handler.handleMessage(this);

	protected Message() {
		// do nothing
	}

	protected Message(MessageType messageType, int payloadSize) {
		final ByteBuffer buffer = ByteBuffer.allocate(this.getPayloadOffset() + payloadSize);
		this.wrap(buffer);
		this.setMessageType(messageType);
	}
	
	public Message wrap(ByteBuffer buffer) {
		this.buffer = buffer;
		return this;
	}

	public short getSenderId() {
		return this.getBuffer().getShort(HeaderOffsets.SenderId);
	}

	public Message setSenderId(short senderId) {
		this.getBuffer().putShort(HeaderOffsets.SenderId, senderId);
		return this;
	}

	public short getDestinationId() {
		return this.getBuffer().getShort(HeaderOffsets.DestinationId);
	}

	public Message setDestinationId(short destinationId) {
		this.getBuffer().putShort(HeaderOffsets.DestinationId, destinationId);
		return this;
	}
	
	public MessageType getMessageType() {
		return getMessageType(this.getBuffer());
	}

	protected Message setMessageType(MessageType messageType) {
		this.buffer.putShort(HeaderOffsets.MessageType, messageType.getMessageTypeCode());
		return this;
	}
	
	public static MessageType getMessageType(ByteBuffer buffer) {
		final short messageTypeCode = buffer.getShort(HeaderOffsets.MessageType);
		return MessageType.getByCode(messageTypeCode);
	}

	public short getPayloadOffset() {
		return HeaderOffsets.PayloadOffset;
	}

	public ByteBuffer getBuffer() {
		return this.buffer;
	}

	public Message clearPayload() {
		this.buffer.position(HeaderOffsets.PayloadOffset);
		this.buffer.limit(HeaderOffsets.PayloadOffset);
		return this;
	}

	public Runnable getRunnableHandle() {
		assert this.handler != null : "Unbound runnable handle";
		return this.runnableHandle;
	}

	public void bound(MessageHandler handler) {
		this.handler = handler;
	}

	public static <T extends Message> T makeResponse(T inMessage) {
		final short dest = inMessage.getDestinationId();
		inMessage.setDestinationId(inMessage.getSenderId());
		inMessage.setSenderId(dest);
		return inMessage;
	}

	public void toStream(PrintStream out) {
		out.print(this.getMessageType());
		out.print("\tFrom: ");
		out.print(this.getSenderId());
		out.print("\tTo: ");
		out.print(this.getDestinationId());
	}
}
