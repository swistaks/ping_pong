package com.kra.play.infra;

import com.kra.play.pingpong.PingPongMessageDispatcher;

/**
 * Implements in-process messaging system 
 *
 */
public class LocalMessagingContext extends AbstractMessagingContext {

	private final LocalMessageSender localMessageSender;

	public LocalMessagingContext(Player... localPlayers) {
		final PlayerResolver localResolver = new SimplePlayerResolver(localPlayers);
		this.localMessageSender = new LocalMessageSender(localResolver, new PingPongMessageDispatcher(this));
		this.getRouters().add(new SendingMessageRouter(this.localMessageSender));
	}

	@Override
	public void shutDown() {
		this.localMessageSender.shutDown();
		super.shutDown();
	}

	public void waitAllTasksDone() {
		this.localMessageSender.waitAllTasksDone();
	}

	public MessageSender getSender() {
		return this.localMessageSender;
	}

}
