package com.kra.play.infra;

/**
 * Base class for all players 
 *
 */
public abstract class Player {

	private final short id;

	public Player(short id) {
		this.id = id;
	}

	public short getId() {
		return this.id;
	}
	
}
