package com.kra.play.infra;

import java.io.PrintStream;

/**
 * This router prints the message
 *
 */
public class LoggingMessageRouter implements MessageRouter {

	private final PrintStream ps;

	public LoggingMessageRouter(PrintStream ps) {
		this.ps = ps;
	}

	@Override
	public synchronized boolean route(Message message) {
		message.toStream(this.ps);
		this.ps.println();
		return true;
	}

}
