package com.kra.play.infra;

/**
 * Tries to deliver a message using a chain of senders. 
 * 
 */
public class CompositeMessageSender implements MessageSender {

	private final MessageSender[] senders;

	public CompositeMessageSender(MessageSender... senders) {
		super();
		this.senders = senders;
	}

	@Override
	public boolean send(Message message) {
		for (final MessageSender sender : this.senders) {
			if (sender.send(message)) {
				return true;
			}
		}
		return false;
	}

}
