package com.kra.play.infra;

import java.util.Deque;
import java.util.LinkedList;
import java.util.function.Consumer;

/**
 * Implements high-level interface to messaging system
 *
 */
public abstract class AbstractMessagingContext implements MessageSender, MessagingContext{

	private final Deque<MessageRouter> routers = new LinkedList<>();
	private Consumer<AbstractMessagingContext> shutDownListener;
	
	/* (non-Javadoc)
	 * @see com.kra.play.infra.MessagingContext#send(com.kra.play.infra.Message)
	 */
	@Override
	public boolean send(Message message) {
		for (final MessageRouter router : this.getRouters()) {
			if (!router.route(message)) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see com.kra.play.infra.MessagingContext#shutDown()
	 */
	@Override
	public void shutDown() {
		if (this.getShutDownListener() != null) {
			this.getShutDownListener().accept(this);
		}
	}
	
	public Deque<MessageRouter> getRouters() {
		return this.routers;
	}

	public Consumer<AbstractMessagingContext> getShutDownListener() {
		return this.shutDownListener;
	}

	public void setShutDownListener(Consumer<AbstractMessagingContext> shutDownListener) {
		this.shutDownListener = shutDownListener;
	}

	
}
