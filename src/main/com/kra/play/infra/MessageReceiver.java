package com.kra.play.infra;

/**
 * Ancestors of this class implement different ways of receiveing remote messages - e.g. socket, mmf etc 
 *
 */
public class MessageReceiver {

	private final MessageSender sender;

	public MessageReceiver(MessageSender sender) {
		this.sender = sender;
	}

	public MessageSender getSender() {
		return this.sender;
	}
	
}
