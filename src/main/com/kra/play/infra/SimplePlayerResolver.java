package com.kra.play.infra;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Resolves player by id using players list  
 *
 */
public class SimplePlayerResolver extends PlayerResolver {

	// Since we do not have a good primitive [short->Object] map...
	private final short[] ids;
	private final Player[] players;

	public SimplePlayerResolver(Player[] players) {
		this.ids = new short[players.length];
		this.players = players;
		for (int i = 0; i < players.length; i++) {
			this.ids[i] = players[i].getId();
		}
		Arrays.sort(this.ids);
		Arrays.sort(players, Comparator.comparing(Player::getId));
	}

	@Override
	public Player getById(short playerId) {
		final int idx = Arrays.binarySearch(this.ids, playerId);
		if (idx >= 0) {
			return this.players[idx];
		}
		return null;
	}

}
