package com.kra.play.config;

import java.util.Map;

import com.kra.play.pingpong.MessageType;

public class MessageConfig {

	private MessageType type;
	private short destinationPlayerId;
	private Map<String, String> params;
	
	public MessageType getType() {
		return this.type;
	}

	public void setType(MessageType type) {
		this.type = type;
	}

	public Map<String, String> getParams() {
		return this.params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public short getDestinationPlayerId() {
		return destinationPlayerId;
	}

	public void setDestinationPlayerId(short destinationPlayerId) {
		this.destinationPlayerId = destinationPlayerId;
	}
	
}
