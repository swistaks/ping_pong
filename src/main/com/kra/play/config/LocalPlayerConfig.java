package com.kra.play.config;

public class LocalPlayerConfig extends PlayerConfig {

	private String playerClass;

	public String getPlayerClass() {
		return this.playerClass;
	}

	public void setPlayerClass(String playerClass) {
		this.playerClass = playerClass;
	}

}
