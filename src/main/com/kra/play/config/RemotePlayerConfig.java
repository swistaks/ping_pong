package com.kra.play.config;

public class RemotePlayerConfig extends PlayerConfig {

	// TODO refactor to some "endpoint" config
	private String host;
	private int port;

	private String description;

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return this.port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
