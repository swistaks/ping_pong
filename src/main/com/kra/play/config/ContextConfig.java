package com.kra.play.config;

import java.util.List;

public class ContextConfig {

	private List<LocalPlayerConfig> localPlayers;
	private List<RemotePlayerConfig> remotePlayers;
	private List<MessageConfig> initMessages;

	private int receiverPort;
	
	public List<LocalPlayerConfig> getLocalPlayers() {
		return this.localPlayers;
	}

	public void setLocalPlayers(List<LocalPlayerConfig> players) {
		this.localPlayers = players;
	}

	public List<RemotePlayerConfig> getRemotePlayers() {
		return this.remotePlayers;
	}

	public void setRemotePlayers(List<RemotePlayerConfig> remotePlayers) {
		this.remotePlayers = remotePlayers;
	}

	public List<MessageConfig> getInitMessages() {
		return this.initMessages;
	}

	public void setInitMessages(List<MessageConfig> initMessages) {
		this.initMessages = initMessages;
	}

	public int getReceiverPort() {
		return this.receiverPort;
	}

	public void setReceiverPort(int receiverPort) {
		this.receiverPort = receiverPort;
	}

}
