package com.kra.play.util;

import java.io.PrintStream;
import java.nio.ByteBuffer;

/**
 * Defines subrange of bytes. 
 * Primarily used for working with char sequences inside larger binary object 
 *
 */
public class ByteSlice {

	private byte[] array;
	private int offset;
	private int length;

	public static ByteSlice allocate(int size) {
		return (new ByteSlice()).wrap(new byte[size], 0, size);
	}

	public static ByteSlice fromString(String string) {
		final byte[] bytes = string.getBytes();
		return new ByteSlice().wrap(bytes, 0, bytes.length);
	}

	public ByteSlice wrap(byte[] array, int offset, int length) {
		this.array = array;
		this.offset = offset;
		this.length = length;
		return this;
	}

	public int getLength() {
		return this.length;
	}

	public ByteSlice wrap(ByteBuffer buffer, int offset, int length) {
		return this.wrap(buffer.array(), buffer.arrayOffset() + offset, length);
	}

	public void writeTo(ByteBuffer buffer) {
		buffer.put(this.array, this.offset, this.length);
	}

	public void writeTo(ByteBuffer buffer, int bufferPosition) {
		buffer.position(bufferPosition);
		this.writeTo(buffer);
	}

	public ByteSlice putUnsignedAsChars(int value) {
		int digitsCount = 0;
		final int absValue = Math.abs(value);
		int rest = absValue;
		do {
			rest = rest / 10;
			digitsCount++;
		} while (rest > 0);
		rest = absValue;
		for (int i = digitsCount - 1; i >= 0; i--) {
			this.array[this.offset + i] = (byte) (rest % 10 + '0');
			rest = rest / 10;
		}
		this.length = digitsCount;
		return this;
	}

	public void toStream(PrintStream out) {
		out.write(this.array, this.offset, this.length);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == this) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		final ByteSlice thatSlice = (ByteSlice) obj;
		return ByteTools.equals(this.array, this.offset, this.length, thatSlice.array, thatSlice.offset, thatSlice.length);
	}

	@Override
	public int hashCode() {
		if (this.array == null) {
			return 0;
		}

		int hash = 1;
		for (int i = this.offset; i < this.offset + this.length; i++) {
			hash = 31 * hash + this.array[i];
		}

		return hash;
	}
	
	@Override
	public String toString() {
		return new String(this.array, this.offset, this.length);
	}
}
