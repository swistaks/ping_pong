package com.kra.play.util;

import java.nio.ByteBuffer;

/**
 * Low level utility methods 
 */
public abstract class ByteTools {

	public static ByteBuffer clone(ByteBuffer original) {
		final ByteBuffer clone = ByteBuffer.allocate(original.capacity());
		final int oldPos = original.position();
		original.position(0);
		clone.put(original);
		original.position(oldPos);
		clone.position(original.position());
		clone.limit(original.limit());
		return clone;
	}
	
	public static boolean equals(byte[] a, int offsetA, int lengthA, byte[] a2, int offsetA2, int lengthA2) {
		if (a == a2) {
			return true;
		}
		if (a == null || a2 == null) {
			return false;
		}

		final int length = lengthA;
		if (lengthA2 != length) {
			return false;
		}

		for (int i = 0; i < length; i++) {
			if (a[offsetA + i] != a2[offsetA2 + i]) {
				return false;
			}
		}

		return true;
	}
	
}
