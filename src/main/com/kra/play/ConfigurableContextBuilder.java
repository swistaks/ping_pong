package com.kra.play;

import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.kra.play.config.ContextConfig;
import com.kra.play.config.LocalPlayerConfig;
import com.kra.play.config.MessageConfig;
import com.kra.play.config.RemotePlayerConfig;
import com.kra.play.infra.LocalMessagingContext;
import com.kra.play.infra.LoggingMessageRouter;
import com.kra.play.infra.Message;
import com.kra.play.infra.Player;
import com.kra.play.infra.remote.NioMessagingContext;
import com.kra.play.infra.remote.RemotePlayer;
import com.kra.play.pingpong.InitMessage;
import com.kra.play.pingpong.MessageType;
import com.kra.play.pingpong.PingPongInitiator;
import com.kra.play.pingpong.PingPongPlayer;
import com.kra.play.util.ByteSlice;

/**
 * Builds messaging context basing on configuration provided 
 *
 */
public class ConfigurableContextBuilder {

	private final ContextConfig config;

	public ConfigurableContextBuilder(ContextConfig config) {
		this.config = config;
	}

	public MessagingContextManager buildContextManager() {
		final boolean isDistributedContext = this.config.getRemotePlayers() != null && !this.config.getRemotePlayers().isEmpty();
		// TODO we may delegate this to two build strategies
		return isDistributedContext ? this.createDistibutedManager() : this.createLocalManager();
	}

	private MessagingContextManager createLocalManager() {
		final LocalMessagingContext context = this.createLocalContext();
		final List<Message> initMessages = this.getInitMesssages();
		return new LocalContextManager(context, initMessages);
	}

	private MessagingContextManager createDistibutedManager() {
		final NioMessagingContext context = this.createRemoteContext();
		final List<Message> initMessages = this.getInitMesssages();
		return new DistributedContextManager(context, initMessages);
	}

	private List<Message> getInitMesssages() {
		if (this.config.getInitMessages() == null) {
			return Collections.emptyList();
		}

		return this.config.getInitMessages().stream().map(this::createMessage).collect(Collectors.toList());
	}

	private Message createMessage(MessageConfig msgConfig) {
		// TODO replace with a factory
		if (msgConfig.getType().equals(MessageType.InitMessage)) {
			final short messageDestination = msgConfig.getDestinationPlayerId();
			final short targetPlayerId = Short.parseShort(msgConfig.getParams().get("targetPlayerId"));
			final ByteSlice initTextMessage = ByteSlice.fromString(msgConfig.getParams().get("initTextMessage"));
			final int stopCondition = Integer.parseInt(msgConfig.getParams().get("stopCondition"));
			return new InitMessage((short) -1, messageDestination, targetPlayerId, stopCondition, initTextMessage);
		} else {
			throw new RuntimeException("Message type not supported");
		}
	}

	private LocalMessagingContext createLocalContext() {
		final Player[] localPlayers = this.createLocalPlayers();
		final LocalMessagingContext context = new LocalMessagingContext(localPlayers);
		context.getRouters().addFirst(new LoggingMessageRouter(System.out));
		return context;
	}

	private NioMessagingContext createRemoteContext() {
		final Player[] localPlayers = this.createLocalPlayers();
		final RemotePlayer[] remotePlayers = this.createRemotePlayers();
		final NioMessagingContext context = new NioMessagingContext(localPlayers, remotePlayers, this.config.getReceiverPort());
		context.getRouters().addFirst(new LoggingMessageRouter(System.out));
		return context;
	}

	private RemotePlayer[] createRemotePlayers() {
		return this.config.getRemotePlayers().stream().map(this::createPlayer).toArray(RemotePlayer[]::new);
	}

	private Player[] createLocalPlayers() {
		return this.config.getLocalPlayers().stream().map(this::createPlayer).toArray(Player[]::new);
	}

	public Player createPlayer(LocalPlayerConfig playerConf) {
		// TODO replace with a factory
		if (PingPongInitiator.class.getSimpleName().equals(playerConf.getPlayerClass())) {
			return new PingPongInitiator(playerConf.getId());
		} else if (PingPongPlayer.class.getSimpleName().equals(playerConf.getPlayerClass())) {
			return new PingPongPlayer(playerConf.getId());
		} else {
			throw new RuntimeException("Player class not supported");
		}
	}

	public Player createPlayer(RemotePlayerConfig playerConf) {
		return new RemotePlayer(playerConf.getId(), new InetSocketAddress(playerConf.getHost(), playerConf.getPort()));
	}

}
