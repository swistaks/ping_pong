Build:

mvn package

Run:

cd ./target/bin

a) In-Process Ping Pong game:

runInProcess.sh

b) Ping Pong game between two processes:

runDistributed.sh


